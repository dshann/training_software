import glob, os
import random as r

dataset_path = '/Users/davidshannon/Desktop/labelImg-master/training_images'

# Create and/or truncate train.txt and test.txt
file_train = open('train.txt', 'w')  
file_test = open('test.txt', 'w')

# Populate train.txt and test.txt
counter = -1  
for pathAndFilename in glob.iglob(os.path.join(dataset_path, "*.jpg")):  
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))

    counter = r.randint(0,10)
    if counter == 0:
        file_test.write(dataset_path + "/" + title + '.jpg' + "\n")
    else:
        file_train.write(dataset_path + "/" + title + '.jpg' + "\n")
counter = -1
for pathAndFilename in glob.iglob(os.path.join(dataset_path, "*.JPG")):
    title, ext = os.path.splitext(os.path.basename(pathAndFilename))
    
    counter = r.randint(0,10)
    if counter == 0:
        file_test.write(dataset_path + "/" + title + '.jpg' + "\n")
    else:
        file_train.write(dataset_path + "/" + title + '.jpg' + "\n")
file_train.close()
file_test.close()
