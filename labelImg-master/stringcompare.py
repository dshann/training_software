import os, glob

print("\n")
# names and extensions
img_names = []
img_ext = []
lbl_names = []
lbl_ext = []

for file in os.listdir("/Users/davidshannon/Desktop/labelImg-master/training_images"):
    img_names.append(file[:len(file)-4])
    img_ext.append(file[len(file)-3:])

for file in os.listdir("/Users/davidshannon/Desktop/labelImg-master/training_labels"):
    lbl_names.append(file[:len(file)-4])
    lbl_ext.append(file[len(file)-3:])


# file type outliers
bad_img_types = []
bad_label_types = []

for i in range(len(img_names)):
    if not img_ext[i].lower() == "jpg":
        bad_img_types.append(img_names[i])
        
for i in range(len(lbl_names)):
    if not lbl_ext[i] == "txt":
        bad_label_types.append(lbl_names[i])
        
print("Files with bad image types")
for file in bad_img_types:
    print(file)
print()
print("Files with bad label types")
for file in bad_label_types:
    print(file)
print()

# no name matches
no_match_img = []
no_match_lbl = []

for file in img_names:
    if not file in lbl_names:
        no_match_img.append(file)

for file in lbl_names:
    if not file in img_names:
        no_match_lbl.append(file)

print("No-match in img's folder:")
for file in no_match_img:
    print(file)
print()
print("No-match in lbl's folder:")
for file in no_match_lbl:
    print(file)
print()

#print(img_names)
#print(img_ext)
#print(lbl_names)
#print(lbl_ext)
